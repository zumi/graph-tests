package com.one.of.us.reader;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.GZIPInputStream;

public class UrlReader {

	public static String getPage(String url) {
		try {
			URL urlObj = new URL(url);
			URLConnection con = urlObj.openConnection();

			con.addRequestProperty("Accept-encoding", "gzip"); 
			
			con.setDoOutput(true); // we want the response
			con.connect();

			GZIPInputStream gzis = new GZIPInputStream(con.getInputStream());
			InputStreamReader reader = new InputStreamReader(gzis);
			BufferedReader in = new BufferedReader(reader);

			StringBuilder response = new StringBuilder();
			String inputLine;

			String newLine = System.getProperty("line.separator");
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine + newLine);
			}

			in.close();

			return response.toString();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static String getWriteResult(String url) {
		try {
			URL urlObj = new URL(url);
			URLConnection con = urlObj.openConnection();

			con.setDoOutput(true); // we want the response
			con.connect();

			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));

			StringBuilder response = new StringBuilder();
			String inputLine;

			String newLine = System.getProperty("line.separator");
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine + newLine);
			}

			in.close();

			return response.toString();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
