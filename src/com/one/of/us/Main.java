package com.one.of.us;

import com.one.of.us.field.Field;
import com.one.of.us.finder.PathFinder;
import com.one.of.us.parser.GameInfoParser;
import com.one.of.us.reader.UrlReader;

public class Main {

	public static void main(String[] args) {

		GameInfoParser parser = new GameInfoParser(UrlReader.getPage("http://www.hacker.org/oneofus/index.php?name=zumi&password=hac24lenvetli"));
		Field field = new Field(parser.getX(), parser.getY(), parser.getFieldString());
		field.addConnections();
		field.printField();

		PathFinder finder = new PathFinder(field);
		String path = finder.findPath();
		// System.out.println("=== Solution:");
		// System.out.println(path);
		// UrlReader.getPage("http://www.hacker.org/oneofus/index.php?name=zumi&password=hac24lenvetli&path="
		// + path);
		// }
	}

}
