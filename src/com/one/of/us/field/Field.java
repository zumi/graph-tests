package com.one.of.us.field;

import java.util.ArrayList;
import java.util.Iterator;

import org.graphstream.graph.EdgeRejectedException;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;

public class Field {

	private Node[][] array;
	private int x;
	private int y;
	private int ident = 0;
	

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Field(int x, int y, String board) {
		this.x = x;
		this.y = y;
		array = new Node[x][y];
		int rowOffset = 0;
		for (int i = 0; i < y; i++) {
			String row = board.substring(rowOffset, rowOffset + (x * 2));
			rowOffset += (x * 2);
			int tempX = 0;
			for (int j = 0; j < (x * 2); j += 2) {
				array[tempX][i] = new Node(row.charAt(j) + "", row.charAt(j + 1) + "", tempX, i);
				tempX++;
			}
		}
	}

	public void addConnections() {
		for (int y = 0; y < this.y; y++) {
			for (int x = 0; x < this.x; x++) {
				Node current = array[x][y];

				for (int x2 = x + 1; x2 < this.x; x2++) {
					if (current.isCompatible(array[x2][y])) {
						current.addConnection(array[x2][y]);
					}
				}
				for (int y2 = y + 1; y2 < this.y; y2++) {
					if (current.isCompatible(array[x][y2])) {
						current.addConnection(array[x][y2]);
					}
				}
			}
		}
	}

	public Node getNodeWithLeastConnections(ArrayList<Node> invalid) {
		Node current = array[0][0];
		for (int y = 0; y < this.y; y++) {
			for (int x = 0; x < this.x; x++) {
				if ((current.getConCount() > array[x][y].getConCount()) && !invalid.contains(array[x][y])) {
					current = array[x][y];
				}
			}
		}
		return current;
	}

	public void reset() {
		for (int i = 0; i < y; i++) {
			for (int j = 0; j < x; j++) {
				array[j][i].reset();
			}
		}
	}

	public void printGraph(Node[] path, int pointer) {
		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		Graph graph = new SingleGraph("Graph" + ident);
		ident++;
		graph.clear();

		for (int i = 0; i < y; i++) {
			for (int j = 0; j < x; j++) {
				graph.addNode(array[j][i].getNodeAsString());
			}
		}
		for (int i = 0; i < y; i++) {
			for (int j = 0; j < x; j++) {
				String node1 = array[j][i].getNodeAsString();
				Iterator<Node> iterator = array[j][i].getConnections().iterator();
				while (iterator.hasNext()) {
					Node node2 = iterator.next();
					try {
						graph.addEdge(node1 + node2.getNodeAsString(), node1, node2.getNodeAsString());
					} catch (EdgeRejectedException ex) {

					}
				}
			}
		}

		if (pointer > 0) {
			for (int n = 1; n <= pointer; n++) {
				try {
					graph.addEdge(path[n - 1].getNodeAsString() + path[n].getNodeAsString(), path[n - 1].getNodeAsString(), path[n].getNodeAsString());
				} catch (EdgeRejectedException ex) {

				}
			}
		}

		graph.getNode(path[pointer].getNodeAsString()).addAttribute("ui.style", "fill-color: rgb(255,0,0);");

		graph.display();
	}

	public void printField() {
		System.out.println("=== Field:");
		for (int i = 0; i < y; i++) {
			for (int j = 0; j < x; j++) {
				array[j][i].printNode();
				System.out.print("  ");
			}
			System.out.println();
		}
	}
}
