package com.one.of.us.field;

import java.util.Iterator;
import java.util.TreeSet;

public class Node implements Comparable<Node> {

	private String shape;
	private String color;
	private int x;
	private int y;
	private TreeSet<Node> connections = new TreeSet<Node>();
	private TreeSet<Node> validConnections = new TreeSet<>();
	private int conCount = 0;

	// private int currCon = 0;

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public String getShape() {
		return shape;
	}

	public String getColor() {
		return color;
	}

	public int getConCount() {
		return conCount;
	}

	public TreeSet<Node> getConnections() {
		return validConnections;
	}

	public int getValidConnectionCount() {
		return validConnections.size();
	}

	public Node(String shape, String color, int x, int y) {
		this.shape = shape;
		this.color = color;
		this.x = x;
		this.y = y;
	}

	public boolean isCompatible(Node node) {
		return this.shape.equals(node.getShape()) || this.color.equals(node.getColor());
	}

	public void addConnection(Node node) {
		if (!validConnections.contains(node)) {
			connections.add(node);
			validConnections.add(node);
			node.addConnection(this); // bidirectional
			conCount++;
			// currCon++;
		}
	}

	public void removeConnection(Node node) {
		validConnections.remove(node);
	}

	public Node getNextNode() {
		Node candidate = findConnectionWithLeastConnections();
		Iterator<Node> iterator = validConnections.iterator();
		while (iterator.hasNext()) {
			Node next = iterator.next();
			if (!candidate.equals(next) && next.getValidConnectionCount() <= 2) {
				return null; // return null, since the move would isolate a node
			}
		}

		// Next node found, now remove connections
		// remove one connection of all currently valid connections of the
		// corresponding neighbor node
		Iterator<Node> iterator2 = validConnections.iterator();
		while (iterator2.hasNext()) {
			Node next = iterator2.next();
			next.removeConnection(this);
		}
		// remove all connections of the current node (this)
		this.validConnections.clear();

		return candidate;
	}

	public Node findConnectionWithLeastConnections() {
		Iterator<Node> iterator = validConnections.iterator();
		Node current = iterator.next();
		while (iterator.hasNext()) {
			Node next = iterator.next();
			if (current.getValidConnectionCount() > next.getValidConnectionCount()) {
				current = next;
			}
		}
		return current;
	}

	// public Node getNextNode(Node[] path, int pointer) {
	// int maxTries = conCount;
	// while (maxTries >= 0) {
	// maxTries--;
	// Node connection = connections.get(currCon);
	// if (!isInPath(path, pointer, connection)) {
	// return connection;
	// } else {
	// if (currCon == conCount - 1) {
	// // currCon = 0;
	// return null;
	// } else {
	// currCon++;
	// }
	// }
	// }
	// return null;
	// }

	public void reset() {
		this.validConnections.addAll(connections);
		Iterator<Node> iterator2 = validConnections.iterator();
		while (iterator2.hasNext()) {
			Node next = iterator2.next();
			next.addConnection(this);
		}
	}

	public void printNode() {
		System.out.print(shape + ":" + color + ":" + conCount);
	}

	public String getNodeAsString() {
		return shape + ":" + color + ":" + conCount + ":" + x + ":" + y;
	}

	@Override
	public int compareTo(Node arg0) {
		return this.conCount - arg0.conCount;
	}

}
