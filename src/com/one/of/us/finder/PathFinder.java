package com.one.of.us.finder;

import java.util.ArrayList;

import com.one.of.us.field.Field;
import com.one.of.us.field.Node;

public class PathFinder {

	private Field field;
	private Node[] path;

	public PathFinder(Field field) {
		this.field = field;
		path = new Node[field.getX() * field.getY()];
	}

	public String findPath() {
		int pointer = 0;
		ArrayList<Node> startPoints = new ArrayList<Node>();
		Node currentNode = field.getNodeWithLeastConnections(startPoints);
		currentNode.printNode();
		startPoints.add(currentNode);
		path[pointer] = currentNode;
		while (pointer < path.length - 1) {

			// System.out.println("pointer: " + pointer);
			Node nextNode = path[pointer].getNextNode();
			if (nextNode != null) {
				path[++pointer] = nextNode;
//				field.printGraph(path, pointer);
			} else {
				path[pointer].reset();
				pointer--;

			}

			if (pointer == 0) {
				System.out.println("0");
				Node newStartPoint = field.getNodeWithLeastConnections(startPoints);
				field.reset();
				newStartPoint.printNode();
				path[pointer] = newStartPoint;
				startPoints.add(newStartPoint);
			} else if (pointer < 0) {
				System.out.println("something went very wrong");
			}
		}
		return stringPath();
	}

	public String stringPath() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < path.length; i++) {
			sb.append(path[i].getX());
			sb.append(",");
			sb.append(path[i].getY());
			if (i != path.length - 1) {
				sb.append("_");
			}
		}
		return sb.toString();
	}

	public void printPath() {
		System.out.println("=== Path: ");
		for (int i = 0; i < path.length; i++) {
			System.out.print(i + 1 + ":");
			path[i].printNode();
			System.out.print("  ");
		}
		System.out.println();
	}
}
