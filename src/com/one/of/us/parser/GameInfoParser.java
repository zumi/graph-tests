package com.one.of.us.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GameInfoParser {

	private String fieldString = "";
	private int x = 0;
	private int y = 0;

	public String getFieldString() {
		return fieldString;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	// x=3&y=3&board=BaBcAaBcBbAbCcCaAb

	public GameInfoParser(String str) {

		this.x = Integer.parseInt(cutAndReplace(applyRegex("x=.*?&", str)));
		this.y = Integer.parseInt(cutAndReplace(applyRegex("y=.*?&", str)));
		this.fieldString = cutAndReplace(applyRegex("board=.*?\"", str));
		System.out.println("=== Parser: ");
		System.out.println("x: " + x);
		System.out.println("y: " + y);
		System.out.println("board: " + fieldString);
	}

	public String cutAndReplace(String string) {
		String value = string.split("=")[1];
		return value.replace("&", "");
	}

	public String applyRegex(String regex, String string) {
		Pattern regexPattern = Pattern.compile(regex);
		regexPattern.matcher(string);
		Matcher matcher = regexPattern.matcher(string);
		matcher.find();
		return matcher.group();
	}

}
